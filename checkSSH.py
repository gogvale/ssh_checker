#!/usr/bin/env python3
import warnings
warnings.filterwarnings("ignore")

IP = 'DireccionIP'
User = 'Usuario'
Pw = 'Password'

import pandas as pd
import paramiko

df = pd.read_excel('Libro1.xlsx', dtype={IP:str, User:str, Pw:str})
df = df[[IP,User,Pw]]

def getSSH(ip,user,pw):
	print('connecting to IP {} (...)'.format(ip))
	ssh = paramiko.SSHClient()
	ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
	try:
		print('Logging in...',end='')
		try:
			ssh.connect(ip, username=user, password=pw,timeout=5)
			print('ok')
			return [True]
		except paramiko.ssh_exception.AuthenticationException:
			ssh.connect(ip, username='cli', password=user,timeout=5)
			print('no se pudo verificar user y pw')
			return [False,'ocupa "cli"']	
	except Exception as e:
		print('error: {}'.format(e))
		return [False,e]
work = set()
nowork = dict()

for j in range(len(df)):
	i = df.iloc[j]
	a = getSSH(i[IP],i[User],i[Pw])
	if len(a) == 1:
		work.add(i[IP])
	else:
		nowork[i[IP]] = a[1]
	print('{}/{} ({}%)'.format(j+1,len(df),round(100*(j+1)/len(df),2)))

print('Finished!')
print('Successful: {}'.format(len(work)))
print('Unsuccessful: {}'.format(len(nowork)))

with open('Not Working.txt','w') as f:
	for i in nowork:
		f.write('{}:\t{}\n'.format(i,nowork[i]))
with open('Working.txt','w') as f:
	for i in work:
		f.write('{}\n'.format(i))
